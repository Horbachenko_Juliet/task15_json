package com.epam.model.parser;

import com.epam.model.Bank;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;

public class GsonParser {
    private Gson gson = new Gson();

    public Bank[] parseBanks(String jsonFilePath) throws FileNotFoundException {
        JsonReader reader = new JsonReader(
                new InputStreamReader(
                        new FileInputStream(jsonFilePath)
                )
        );
        reader.setLenient(true);
        return gson.fromJson(reader, Bank[].class);
    }
}
