package com.epam.model;

public class Bank {

    private String name;
    private String registrationCountry;
    private String depositorName;
    private Account account;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegistrationCountry() {
        return registrationCountry;
    }

    public void setRegistrationCountry(String registrationCountry) {
        this.registrationCountry = registrationCountry;
    }

    public String getDepositorName() {
        return depositorName;
    }

    public void setDepositorName(String depositorName) {
        this.depositorName = depositorName;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public String toString() {
       return "Bank name: " +  this.name + ", registration: " +  this.registrationCountry + ", depositor: " +
               this.depositorName + ", account info: " +  this.account;
    }
}
